# rm(list=ls())
# cat("\014")
# 
# script.dir <- dirname(sys.frame(1)$ofile)
# setwd(script.dir)

library('tidyverse')
library('stringr')

data_folder <- readline(prompt="Enter folder of data: ")
# data_folder <- "test"
folder <- list.files(data_folder)
folder <- str_to_lower(folder)

############### Function retrieve data from files ################
RetrieveData <- function(condition_name){
  files_condition <-  list()
  for (filename in folder){
    if (!is.na(str_locate(filename,condition_name)[1])){
      files_condition = append(files_condition,filename)
    }
  }
  
  for (i in 1:length(files_condition)){
    if (i == 1){
      data_condition <- read_table(str_c(data_folder,'\\', files_condition[i]),col_names = c("AF3","F7","F3","FC5","T7","P7","O1","O2","P8","T8","FC6","F4","F8","AF4"))
    }
    else{
      data_condition <- bind_rows(data_condition, read_table(str_c(data_folder,'\\', files_condition[i]),col_names = c("AF3","F7","F3","FC5","T7","P7","O1","O2","P8","T8","FC6","F4","F8","AF4")))
    }
  }
  
  return(data_condition)
}

neutre_raw <- RetrieveData('neutre')
faible_raw <- RetrieveData('faible')
intense_raw <- RetrieveData('intense')
frisson_raw <- RetrieveData('frisson')

############### Function compute asymmetry ##################
Asymmetry <- function(condition_raw){
  
  condition_asymmetry <-  tibble(channel = character(),
                                 band = character(),
                                 value = numeric())
  
  for (i in 1:(length(condition_raw)/2)-1){
    
    temp <- log10(condition_raw[length(condition_raw)-i]) - log10(condition_raw[1+i])
    label_temp <- paste(colnames(condition_raw[length(condition_raw)-i]),colnames(condition_raw[1+i]),sep='-')
    
    
    for (j in 1:nrow(temp)){
      temp_tibble <- tibble(channel = label_temp,
                            band = paste('freq',j),
                            value = temp[j,1])
      condition_asymmetry <- bind_rows(condition_asymmetry, temp_tibble)
    }
  }
  
  return(condition_asymmetry)
}

neutre_asym <- Asymmetry(neutre_raw)
faible_asym <- Asymmetry(faible_raw)
intense_asym <- Asymmetry(intense_raw)
frisson_asym <- Asymmetry(frisson_raw)

########### Compile asymmetry all bands / all conditions into all_bands_asym ############

all_bands_asym <-  tibble(channel = letters[1:28],
                          band = letters[1:28],
                          neutre = 1:28,
                          faible = 1:28,
                          intense = 1:28,
                          frisson = 1:28)

all_bands_asym['channel'] <- frisson_asym['channel']
all_bands_asym['band'] <- frisson_asym['band']
all_bands_asym['neutre'] <- neutre_asym['value']
all_bands_asym['faible'] <- faible_asym['value']
all_bands_asym['intense'] <- intense_asym['value']
all_bands_asym['frisson'] <- frisson_asym['value']

########### Compute increasing trend of asymmetry all bands into all_bands_asym_inter ############

all_bands_asym_inter <- filter(all_bands_asym, all_bands_asym$faible - all_bands_asym$neutre > 0)
all_bands_asym_inter <- filter(all_bands_asym_inter, all_bands_asym_inter$intense - all_bands_asym_inter$faible > 0)
all_bands_asym_inter <- filter(all_bands_asym_inter, all_bands_asym_inter$frisson-all_bands_asym_inter$intense > 0)

########### Function compute ROI electrodes ############
ROI_4 <- function(condition_raw){
  roi_condition <- tibble(band=character(),value=numeric())
  for (i in 1:4){
    temp_freq <- paste('freq',i)
    temp_value <- log10(rowMeans(condition_raw[i,11:14]))-log10(rowMeans(condition_raw[i,1:4]))
    print(temp_value)
    temp_tibble <- tibble(band=temp_freq,value=temp_value)
    roi_condition <- bind_rows(roi_condition,temp_tibble)
  }
  return(roi_condition)
}

neutre_roi4 <- ROI_4(neutre_raw)
faible_roi4 <- ROI_4(faible_raw)
intense_roi4 <- ROI_4(intense_raw)
frisson_roi4 <- ROI_4(frisson_raw)
roi4 <- bind_cols(neutre_roi4,faible_roi4[,2],intense_roi4[,2],frisson_roi4[,2])
roi4 <- rename(roi4, 'neutre'=value, 'faible'=value1, 'intense'='value2', 'frisson'='value3')

ROI_2 <- function(condition_raw){
  roi_condition <- tibble(band=character(),value=numeric())
  for (i in 1:4){
    temp_freq <- paste('freq',i)
    temp_value <- log10((condition_raw[i,12]+condition_raw[i,14])/2)-log10((condition_raw[i,1]+condition_raw[i,3])/2)
    temp_tibble <- tibble(band=temp_freq,value=temp_value[[1,1]])
    roi_condition <- bind_rows(roi_condition,temp_tibble)
  }
  return(roi_condition)
}

neutre_roi2 <- ROI_2(neutre_raw)
faible_roi2 <- ROI_2(faible_raw)
intense_roi2<- ROI_2(intense_raw)
frisson_roi2 <- ROI_2(frisson_raw)
roi2 <- bind_cols(neutre_roi2,faible_roi2[,2],intense_roi2[,2],frisson_roi2[,2])
roi2 <- rename(roi2, 'neutre'=value, 'faible'=value1, 'intense'='value2', 'frisson'='value3')

CheckROI <- function(roi){
  roi_tibble <- tibble(increasing=1:4)
  for(i in 1:dim(roi)[[1]]){
    indicator <- TRUE
    for(j in 2:(dim(roi)[[2]]-1)){
      indicator <- indicator & (roi[i,j+1]>roi[i,j])
    }
    if(indicator){
      roi_tibble[i,1] <- 1
    }
    else{
      roi_tibble[i,1] <- 0
    }
  }
  return(bind_cols(roi,roi_tibble))
}

roi2 <- CheckROI(roi2)
roi4 <- CheckROI(roi4)

###################### Create output folder ##############################

subDir <- paste(data_folder,'_output_withNeutre',sep='')

if (file.exists(subDir)){
  setwd(file.path(script.dir, subDir))
} else {
  dir.create(file.path(script.dir, subDir))
  setwd(file.path(script.dir, subDir))
}

############## Generate 4 output files for all Asymmetry #####################

AsymmetryAll <- function(freq_band){
  all_channel <- filter(all_bands_asym, all_bands_asym$band == freq_band)
  all_channel <- select(all_channel, -one_of('band'))
  return(all_channel)
}

for (j in 1:4){
  asym_all_channel <-  AsymmetryAll(paste('freq',j))
  write.csv(asym_all_channel , file = paste(data_folder,'_',paste('freq',j),'_asymmetry_all_channel','.csv',sep=''),row.names=FALSE)
}

############## Generate 4 output files for Asymmetry Increasing #####################

AsymmetryIncreasing <- function(freq_band){
  inter_channel <- filter(all_bands_asym_inter, all_bands_asym_inter$band == freq_band)
  inter_channel <- select(inter_channel, -one_of('band'))
  return(inter_channel)
}

for (j in 1:4){
  asym_increasing_channel <-  AsymmetryIncreasing(paste('freq',j))
  write.csv(asym_increasing_channel , file = paste(data_folder,'_',paste('freq',j),'_asymmetry_increasing','.csv',sep=''),row.names=FALSE)
}

############## Generate 2 output files for ROI #####################
write.csv(roi2 , file = paste(data_folder,'_','roi2','.csv',sep=''),row.names=FALSE)
write.csv(roi4 , file = paste(data_folder,'_','roi4','.csv',sep=''),row.names=FALSE)

########### Tidying raw data of all condition ############

TidyRaw <- function(condition_raw){
  condition_tidy <- add_column(condition_raw, band = c('freq 1','freq 2','freq 3', 'freq 4'))
  condition_tidy <- gather(condition_tidy,channel,value,-band)
  condition_tidy <- select(condition_tidy,c(2,1,3))
  return(condition_tidy)
}

neutre_tidy <- TidyRaw(neutre_raw)
faible_tidy <- TidyRaw(faible_raw)
intense_tidy <- TidyRaw(intense_raw)
frisson_tidy <- TidyRaw(frisson_raw)

########### Compile all bands / all conditions into all_bands_conditions ############

all_bands_conditions <-  tibble(channel = letters[1:56],
                                band = letters[1:56],
                                neutre = 1:56,
                                faible = 1:56,
                                intense = 1:56,
                                frisson = 1:56)

all_bands_conditions['channel'] <- frisson_tidy['channel']
all_bands_conditions['band'] <- frisson_tidy['band']
all_bands_conditions['neutre'] <- neutre_tidy['value']
all_bands_conditions['faible'] <- faible_tidy['value']
all_bands_conditions['intense'] <- intense_tidy['value']
all_bands_conditions['frisson'] <- frisson_tidy['value']

############ Function compute whole brain trend #############

WholeBrainTrend <- function(freq, trend){
  freq_trend <- filter(all_bands_conditions, all_bands_conditions$band == freq)
  
  if(trend == 'increasing'){
    freq_trend <- filter(freq_trend, freq_trend$faible - freq_trend$neutre > 0)
    freq_trend <- filter(freq_trend, freq_trend$intense - freq_trend$faible > 0)
    freq_trend <- filter(freq_trend, freq_trend$frisson - freq_trend$intense > 0)
  }
  
  if(trend == 'decreasing'){
    freq_trend <- filter(freq_trend, freq_trend$faible - freq_trend$neutre < 0)
    freq_trend <- filter(freq_trend, freq_trend$intense - freq_trend$faible < 0)
    freq_trend <- filter(freq_trend, freq_trend$frisson - freq_trend$intense < 0)
  }
  
  return(freq_trend)
}

############## Generate 4 output files for Whole Brain Increasing #####################

for (j in 1:4){
  whole_brain_increasing <-  WholeBrainTrend(paste('freq',j), 'increasing')
  write.csv(whole_brain_increasing , file = paste(data_folder,'_',paste('freq',j),'_wholebrain_increasing_channel','.csv',sep=''),row.names=FALSE)
}

############## Generate 4 output files for Whole Brain Decreasing #####################

for (j in 1:4){
  whole_brain_decreasing <-  WholeBrainTrend(paste('freq',j), 'decreasing')
  write.csv(whole_brain_decreasing , file = paste(data_folder,'_',paste('freq',j),'_wholebrain_decreasing_channel','.csv',sep=''),row.names=FALSE)
}


############## Function Compare #####################

CompareCorresponding <- function(whole, asym){
  correspond_channel <- tibble(asymmetry = character(), wholebrain = character())
  if (nrow(whole) > 0 && nrow(asym) > 0){
    for (i in 1:nrow(whole)){
      
      temp_from_whole <- whole[[i,1]]
      
      for (j in 1:nrow(asym)){
        
        temp_from_asym <- asym[[j,1]]
        temp_from_asym_split_left <- strsplit(temp_from_asym, '-')[[1]][1]
        temp_from_asym_split_right <- strsplit(temp_from_asym, '-')[[1]][2]
        
        if(identical(temp_from_asym_split_left,temp_from_whole)){
          temp <- tibble(asymmetry = temp_from_asym, wholebrain = temp_from_whole)
          correspond_channel <- bind_rows(correspond_channel,temp)
        }
        
        if(identical(temp_from_asym_split_right,temp_from_whole)){
          temp <- tibble(asymmetry = temp_from_asym, wholebrain = temp_from_whole)
          correspond_channel <- bind_rows(correspond_channel,temp)
        }
      }
    }
  }
  return(correspond_channel)
}

############## Generate 1 output files compare decreasing-delta-wholebrain with Hi/Lo-alpha-asymmetry #####################

delta_decreasing <- WholeBrainTrend('freq 1', 'decreasing')
low_alpha_asym_increasing <-  AsymmetryIncreasing('freq 3')
# high_alpha_asym_increasing <-  AsymmetryIncreasing('freq 4')

correspond_delta_decreasing_low_alpha <- CompareCorresponding(delta_decreasing, low_alpha_asym_increasing)
write.csv(correspond_delta_decreasing_low_alpha , file = paste(data_folder,'_','_wholebrain_delta_decreasing_compare_asymmetry_alpha','.csv',sep=''),row.names=FALSE)

# correspond_delta_decreasing_hi_alpha <- CompareCorresponding(delta_decreasing, high_alpha_asym_increasing)
# write.csv(correspond_delta_decreasing_hi_alpha , file = paste(data_folder,'_','_wholebrain_delta_decreasing_compare_asymmetry_high_alpha','.csv',sep=''),row.names=FALSE)

############## Generate 1 output files compare increasing-theta-wholebrain with Hi/Lo-alpha-asymmetry #####################

theta_increasing <- WholeBrainTrend('freq 2', 'increasing')

correspond_theta_increasing_low_alpha <- CompareCorresponding(theta_increasing, low_alpha_asym_increasing)
write.csv(correspond_theta_increasing_low_alpha , file = paste(data_folder,'_','_wholebrain_theta_increasing_compare_asymmetry_alpha','.csv',sep=''),row.names=FALSE)

# correspond_theta_increasing_hi_alpha <- CompareCorresponding(theta_increasing, high_alpha_asym_increasing)
# write.csv(correspond_theta_increasing_hi_alpha , file = paste(data_folder,'_','_wholebrain_theta_increasing_compare_asymmetry_high_alpha','.csv',sep=''),row.names=FALSE)

############## Generate 1 output files compare increasing-beta-wholebrain with Hi/Lo-alpha-asymmetry #####################

beta_increasing <- WholeBrainTrend('freq 4', 'increasing')

correspond_beta_increasing_low_alpha <- CompareCorresponding(beta_increasing, low_alpha_asym_increasing)
write.csv(correspond_beta_increasing_low_alpha , file = paste(data_folder,'_','_wholebrain_beta_increasing_compare_asymmetry_alpha','.csv',sep=''),row.names=FALSE)

# correspond_beta_increasing_hi_alpha <- CompareCorresponding(beta_increasing, high_alpha_asym_increasing)
# write.csv(correspond_beta_increasing_hi_alpha , file = paste(data_folder,'_','_wholebrain_beta_increasing_compare_asymmetry_high_alpha','.csv',sep=''),row.names=FALSE)

############## Generate 1 output files compare decreasing-beta-wholebrain with Hi/Lo-alpha-asymmetry #####################

beta_decreasing <- WholeBrainTrend('freq 4', 'decreasing')

correspond_beta_decreasing_low_alpha <- CompareCorresponding(beta_decreasing, low_alpha_asym_increasing)
write.csv(correspond_beta_decreasing_low_alpha , file = paste(data_folder,'_','_wholebrain_beta_decreasing_compare_asymmetry_alpha','.csv',sep=''),row.names=FALSE)

# correspond_beta_decreasing_hi_alpha <- CompareCorresponding(beta_decreasing, high_alpha_asym_increasing)
# write.csv(correspond_beta_decreasing_hi_alpha , file = paste(data_folder,'_','_wholebrain_beta_decreasing_compare_asymmetry_high_alpha','.csv',sep=''),row.names=FALSE)

############# Generate 1 output files compare delta decreasing / theta decreasing / asymmetry ##################

delta_decrease_theta_increase <- bind_rows(delta_decreasing, theta_increasing)
delta_decrease_theta_increase <- delta_decrease_theta_increase %>% group_by(channel)%>% filter(n()>1)
delta_decrease_theta_increase <- unique(delta_decrease_theta_increase[1])

correspond_delta_theta_low_alpha <- CompareCorresponding(delta_decrease_theta_increase, low_alpha_asym_increasing)
write.csv(correspond_delta_theta_low_alpha , file = paste(data_folder,'_','_wholebrain_delta_decreasing_theta_increasing_compare_asymmetry_alpha','.csv',sep=''),row.names=FALSE)

# correspond_delta_theta_hi_alpha <- CompareCorresponding(delta_decrease_theta_increase, high_alpha_asym_increasing)
# write.csv(correspond_delta_theta_hi_alpha , file = paste(data_folder,'_','_wholebrain_delta_decreasing_theta_increasing_compare_asymmetry_high_alpha','.csv',sep=''),row.names=FALSE)

############### Vector for arousal ################
# ("AF3","F7","F3","FC5","T7","P7","O1","O2","P8","T8","FC6","F4","F8","AF4")
#AF3 AF4 F3 F4
combination_1 <- c(1,14,3,12)
#AF3 AF4 F3 F4 F7 F8
combination_2 <- c(1,14,3,12,2,13)
#AF3 AF4 F3 F4 F7 F8 FC5 FC6 T7 T8
combination_3 <- c(1,14,3,12,2,13,4,11,5,10)
#Frontal region
# combination_4 <- c(1,2,3,4,5,6,7,10,11,12,13,14,15,16,18,19,21,22,23,24,25,26,27,28,29,30,32,33,34,35,36,37,38,39,40,41,42,43,46,47,48,49,50,51,54,55,56,61,196,197,205,206,207,212,213,214,215,220,221,222,223,224)
#Central region
# combination_5 <- c(6,7,8,9,15,16,17,23,24,30,41,42,43,44,45,49,50,51,52,53,56,57,58,59,60,62,63,64,65,66,68,72,132,144,155,173,181,182,183,184,185,186,193,194,195,196,197,198,202,203,204,205,206,207,210,211,213,214,215)

############### Function for compute arousal ############### 
ComputeArousal <- function(raw, combination){
  beta <- 0
  alpha <- 0
  for(i in 1:length(combination)){
    alpha <- alpha + raw[3,combination[i]]
    beta <- beta + raw[4,combination[i]]
  }
  return(beta/alpha)
}

final_arousal <- tibble(combination=1:3, neutre=1:3, faible=1:3, intense=1:3, frisson=1:3)

final_arousal[1,2:5] <- c(ComputeArousal(neutre_raw,combination_1), ComputeArousal(faible_raw,combination_1), ComputeArousal(intense_raw,combination_1), ComputeArousal(frisson_raw,combination_1))
final_arousal[2,2:5] <- c(ComputeArousal(neutre_raw,combination_2), ComputeArousal(faible_raw,combination_2), ComputeArousal(intense_raw,combination_2), ComputeArousal(frisson_raw,combination_2))
final_arousal[3,2:5] <- c(ComputeArousal(neutre_raw,combination_3), ComputeArousal(faible_raw,combination_3), ComputeArousal(intense_raw,combination_3), ComputeArousal(frisson_raw,combination_3))
# final_arousal[4,2:5] <- c(ComputeArousal(neutre_raw,combination_4), ComputeArousal(faible_raw,combination_4), ComputeArousal(intense_raw,combination_4), ComputeArousal(frisson_raw,combination_4))
# final_arousal[5,2:5] <- c(ComputeArousal(neutre_raw,combination_5), ComputeArousal(faible_raw,combination_5), ComputeArousal(intense_raw,combination_5), ComputeArousal(frisson_raw,combination_5))

write.csv(final_arousal , file = paste(data_folder,'_','arousal','.csv',sep=''),row.names=FALSE)