# rm(list=ls())
# cat("\014")

# script.dir <- dirname(sys.frame(1)$ofile)
# setwd(script.dir)

library('tidyverse')
library('stringr')

data_folder <- readline(prompt="Enter folder of data: ")
# data_folder <- "LC05_gtec"
folder <- list.files(data_folder)
folder <- str_to_lower(folder)

############### Function retrieve data from files ################
RetrieveData <- function(condition_name){
  files_condition <-  list()
  for (filename in folder){
    if (!is.na(str_locate(filename,condition_name)[1])){
      files_condition = append(files_condition,filename)
    }
  }
  
  for (i in 1:length(files_condition)){
    if (i == 1){
      data_condition <- read_table(str_c(data_folder,'\\', files_condition[i]),col_names = c('F8','F7','F3','F4','AF3','AF4','T7','T8'))
    }
    else{
      data_condition <- bind_rows(data_condition, read_table(str_c(data_folder,'\\', files_condition[i]),col_names = c('F8','F7','F3','F4','AF3','AF4','T7','T8')))
    }
  }
  
  return(data_condition)
}

# neutre_raw <- RetrieveData('neutre')
faible_raw <- RetrieveData('faible')
intense_raw <- RetrieveData('intense')
frisson_raw <- RetrieveData('frisson')

############### Function compute asymmetry ##################

Asymmetry <- function(data_condition){
  data_diff <-  tibble('F8-F7' = 1:4,
                       'F4-F3' = 1:4,
                       'AF4-AF3' = 1:4,
                       'T8-T7' = 1:4)
  data_diff[,1] <- log10(data_condition[1])-log10(data_condition[2])
  data_diff[,2] <- log10(data_condition[4])-log10(data_condition[3])
  data_diff[,3] <- log10(data_condition[6])-log10(data_condition[5])
  data_diff[,4] <- log10(data_condition[8])-log10(data_condition[7])
  # return(data_diff)
  band <- c('freq 1')
  for (j in 2:nrow(frisson_raw)){
    band <- append( band,paste('freq',j))
  }
  data_diff <- add_column( data_diff, band, .before = 1)
  data_diff <- gather(data_diff, 'channel', 'value', 2:5)
  data_diff <-  data_diff[c(2,1,3)]
  return(data_diff)
}

# neutre_asym <- Asymmetry(neutre_raw)
faible_asym <- Asymmetry(faible_raw)
intense_asym <- Asymmetry(intense_raw)
frisson_asym <- Asymmetry(frisson_raw)

########### Compile asymmetry all bands / all conditions into all_bands_asym ############

all_bands_asym <-  tibble(channel = letters[1:16],
                          band = letters[1:16],
                          # neutre = 1:16,
                          faible = 1:16,
                          intense = 1:16,
                          frisson = 1:16)

all_bands_asym['channel'] <- frisson_asym['channel']
all_bands_asym['band'] <- frisson_asym['band']
# all_bands_asym['neutre'] <- neutre_asym['value']
all_bands_asym['faible'] <- faible_asym['value']
all_bands_asym['intense'] <- intense_asym['value']
all_bands_asym['frisson'] <- frisson_asym['value']

########### Compute increasing trend of asymmetry all bands into all_bands_asym_inter ############

# all_bands_asym_inter <- filter(all_bands_asym, all_bands_asym$faible - all_bands_asym$neutre > 0)
# all_bands_asym_inter <- filter(all_bands_asym_inter, all_bands_asym_inter$intense - all_bands_asym_inter$faible > 0)
all_bands_asym_inter <- filter(all_bands_asym, all_bands_asym$intense - all_bands_asym$faible > 0)
all_bands_asym_inter <- filter(all_bands_asym_inter, all_bands_asym_inter$frisson-all_bands_asym_inter$intense > 0)

###################### Create output folder ##############################

subDir <- paste(data_folder,'_output_noNeutre',sep='')

if (file.exists(subDir)){
  setwd(file.path(script.dir, subDir))
} else {
  dir.create(file.path(script.dir, subDir))
  setwd(file.path(script.dir, subDir))
}

############## Generate 4 output files for all Asymmetry #####################

AsymmetryAll <- function(freq_band){
  all_channel <- filter(all_bands_asym, all_bands_asym$band == freq_band)
  all_channel <- select(all_channel, -one_of('band'))
  return(all_channel)
}

for (j in 1:4){
  asym_all_channel <-  AsymmetryAll(paste('freq',j))
  write.csv(asym_all_channel , file = paste(data_folder,'_',paste('freq',j),'_asymmetry_all_channel','.csv',sep=''),row.names=FALSE)
}

############## Generate 4 output files for Asymmetry Increasing #####################

AsymmetryIncreasing <- function(freq_band){
  inter_channel <- filter(all_bands_asym_inter, all_bands_asym_inter$band == freq_band)
  inter_channel <- select(inter_channel, -one_of('band'))
  return(inter_channel)
}

for (j in 1:4){
  asym_increasing_channel <-  AsymmetryIncreasing(paste('freq',j))
  write.csv(asym_increasing_channel , file = paste(data_folder,'_',paste('freq',j),'_asymmetry_increasing','.csv',sep=''),row.names=FALSE)
}

########### Tidying raw data of all condition ############

TidyRaw <- function(condition_raw){
  condition_tidy <- add_column(condition_raw, band = c('freq 1','freq 2','freq 3', 'freq 4'))
  condition_tidy <- gather(condition_tidy,channel,value,-band)
  condition_tidy <- select(condition_tidy,c(2,1,3))
  return(condition_tidy)
}

# neutre_tidy <- TidyRaw(neutre_raw)
faible_tidy <- TidyRaw(faible_raw)
intense_tidy <- TidyRaw(intense_raw)
frisson_tidy <- TidyRaw(frisson_raw)

########### Compile all bands / all conditions into all_bands_conditions ############

all_bands_conditions <-  tibble(channel = letters[1:32],
                                band = letters[1:32],
                                # neutre = 1:32,
                                faible = 1:32,
                                intense = 1:32,
                                frisson = 1:32)

all_bands_conditions['channel'] <- frisson_tidy['channel']
all_bands_conditions['band'] <- frisson_tidy['band']
# all_bands_conditions['neutre'] <- neutre_tidy['value']
all_bands_conditions['faible'] <- faible_tidy['value']
all_bands_conditions['intense'] <- intense_tidy['value']
all_bands_conditions['frisson'] <- frisson_tidy['value']

############ Function compute whole brain trend #############

WholeBrainTrend <- function(freq, trend){
  freq_trend <- filter(all_bands_conditions, all_bands_conditions$band == freq)
  
  if(trend == 'increasing'){
    # freq_trend <- filter(freq_trend, freq_trend$faible - freq_trend$neutre > 0)
    # freq_trend <- filter(freq_trend, freq_trend$intense - freq_trend$faible > 0)
    freq_trend <- filter(freq_trend, freq_trend$intense - freq_trend$faible > 0)
    freq_trend <- filter(freq_trend, freq_trend$frisson - freq_trend$intense > 0)
  }
  
  if(trend == 'decreasing'){
    # freq_trend <- filter(freq_trend, freq_trend$faible - freq_trend$neutre < 0)
    # freq_trend <- filter(freq_trend, freq_trend$intense - freq_trend$faible < 0)
    freq_trend <- filter(freq_trend, freq_trend$intense - freq_trend$faible < 0)
    freq_trend <- filter(freq_trend, freq_trend$frisson - freq_trend$intense < 0)
  }
  
  return(freq_trend)
}

############## Generate 4 output files for Whole Brain Increasing #####################

for (j in 1:4){
  whole_brain_increasing <-  WholeBrainTrend(paste('freq',j), 'increasing')
  write.csv(whole_brain_increasing , file = paste(data_folder,'_',paste('freq',j),'_wholebrain_increasing_channel','.csv',sep=''),row.names=FALSE)
}

############## Generate 4 output files for Whole Brain Decreasing #####################

for (j in 1:4){
  whole_brain_decreasing <-  WholeBrainTrend(paste('freq',j), 'decreasing')
  write.csv(whole_brain_decreasing , file = paste(data_folder,'_',paste('freq',j),'_wholebrain_decreasing_channel','.csv',sep=''),row.names=FALSE)
}


############## Function Compare #####################

CompareCorresponding <- function(whole, asym){
  correspond_channel <- tibble(asymmetry = character(), wholebrain = character())
  
  if (nrow(whole) > 0 && nrow(asym) > 0){
    for (i in 1:nrow(whole)){
      
      temp_from_whole <- whole[[i,1]]
      
      for (j in 1:nrow(asym)){
        
        temp_from_asym <- asym[[j,1]]
        temp_from_asym_split_left <- strsplit(temp_from_asym, '-')[[1]][1]
        temp_from_asym_split_right <- strsplit(temp_from_asym, '-')[[1]][2]
        
        if(identical(temp_from_asym_split_left,temp_from_whole)){
          temp <- tibble(asymmetry = temp_from_asym, wholebrain = temp_from_whole)
          correspond_channel <- bind_rows(correspond_channel,temp)
        }
        
        if(identical(temp_from_asym_split_right,temp_from_whole)){
          temp <- tibble(asymmetry = temp_from_asym, wholebrain = temp_from_whole)
          correspond_channel <- bind_rows(correspond_channel,temp)
        }
      }
    }
  }
  return(correspond_channel)
}

############## Generate 1 output files compare decreasing-delta-wholebrain with Hi/Lo-alpha-asymmetry #####################

delta_decreasing <- WholeBrainTrend('freq 1', 'decreasing')
low_alpha_asym_increasing <-  AsymmetryIncreasing('freq 3')
# high_alpha_asym_increasing <-  AsymmetryIncreasing('freq 4')

correspond_delta_decreasing_low_alpha <- CompareCorresponding(delta_decreasing, low_alpha_asym_increasing)
write.csv(correspond_delta_decreasing_low_alpha , file = paste(data_folder,'_','_wholebrain_delta_decreasing_compare_asymmetry_alpha','.csv',sep=''),row.names=FALSE)

# correspond_delta_decreasing_hi_alpha <- CompareCorresponding(delta_decreasing, high_alpha_asym_increasing)
# write.csv(correspond_delta_decreasing_hi_alpha , file = paste(data_folder,'_','_wholebrain_delta_decreasing_compare_asymmetry_high_alpha','.csv',sep=''),row.names=FALSE)

############## Generate 1 output files compare increasing-theta-wholebrain with Hi/Lo-alpha-asymmetry #####################

theta_increasing <- WholeBrainTrend('freq 2', 'increasing')

correspond_theta_increasing_low_alpha <- CompareCorresponding(theta_increasing, low_alpha_asym_increasing)
write.csv(correspond_theta_increasing_low_alpha , file = paste(data_folder,'_','_wholebrain_theta_increasing_compare_asymmetry_alpha','.csv',sep=''),row.names=FALSE)

# correspond_theta_increasing_hi_alpha <- CompareCorresponding(theta_increasing, high_alpha_asym_increasing)
# write.csv(correspond_theta_increasing_hi_alpha , file = paste(data_folder,'_','_wholebrain_theta_increasing_compare_asymmetry_high_alpha','.csv',sep=''),row.names=FALSE)

############## Generate 1 output files compare increasing-beta-wholebrain with Hi/Lo-alpha-asymmetry #####################

beta_increasing <- WholeBrainTrend('freq 4', 'increasing')

correspond_beta_increasing_low_alpha <- CompareCorresponding(beta_increasing, low_alpha_asym_increasing)
write.csv(correspond_beta_increasing_low_alpha , file = paste(data_folder,'_','_wholebrain_beta_increasing_compare_asymmetry_alpha','.csv',sep=''),row.names=FALSE)

# correspond_beta_increasing_hi_alpha <- CompareCorresponding(beta_increasing, high_alpha_asym_increasing)
# write.csv(correspond_beta_increasing_hi_alpha , file = paste(data_folder,'_','_wholebrain_beta_increasing_compare_asymmetry_high_alpha','.csv',sep=''),row.names=FALSE)

############## Generate 1 output files compare decreasing-beta-wholebrain with Hi/Lo-alpha-asymmetry #####################

beta_decreasing <- WholeBrainTrend('freq 4', 'decreasing')

correspond_beta_decreasing_low_alpha <- CompareCorresponding(beta_decreasing, low_alpha_asym_increasing)
write.csv(correspond_beta_decreasing_low_alpha , file = paste(data_folder,'_','_wholebrain_beta_decreasing_compare_asymmetry_alpha','.csv',sep=''),row.names=FALSE)

# correspond_beta_decreasing_hi_alpha <- CompareCorresponding(beta_decreasing, high_alpha_asym_increasing)
# write.csv(correspond_beta_decreasing_hi_alpha , file = paste(data_folder,'_','_wholebrain_beta_decreasing_compare_asymmetry_high_alpha','.csv',sep=''),row.names=FALSE)

############# Generate 1 output files compare delta decreasing / theta decreasing / asymmetry ##################

delta_decrease_theta_increase <- bind_rows(delta_decreasing, theta_increasing)
delta_decrease_theta_increase <- delta_decrease_theta_increase %>% group_by(channel)%>% filter(n()>1)
delta_decrease_theta_increase <- unique(delta_decrease_theta_increase[1])

correspond_delta_theta_low_alpha <- CompareCorresponding(delta_decrease_theta_increase, low_alpha_asym_increasing)
write.csv(correspond_delta_theta_low_alpha , file = paste(data_folder,'_','_wholebrain_delta_decreasing_theta_increasing_compare_asymmetry_alpha','.csv',sep=''),row.names=FALSE)

# correspond_delta_theta_hi_alpha <- CompareCorresponding(delta_decrease_theta_increase, high_alpha_asym_increasing)
# write.csv(correspond_delta_theta_hi_alpha , file = paste(data_folder,'_','_wholebrain_delta_decreasing_theta_increasing_compare_asymmetry_high_alpha','.csv',sep=''),row.names=FALSE)